# JavaScript Bundler

- A module bundler is a tool that takes pieces of JavaScript and their dependencies and bundles them into a single file
- Two main stages of bundler:
  - dependency resolution
  - packing

## Dependency Resolution

- Usually starts from an entry point (entry file)
  - this is the file that should bootstrap the entire application
- look for all dependencies of the code and construct a **dependency graph**
- During **dependency resolution**, the bundler creaets a **modules map**

## Packing

- packing takes the modules map and emits a single executable JS file
