const fs = require("fs"); // reading file
const { parse } = require("@babel/parser");
const traverse = require("@babel/traverse").default;
const { transformFromAst } = require("@babel/core");
const path = require("path");

let ID = 0;

// A function that accepts path to a file as an arg
// Reads the file's content and extracts its dependencies
function createAsset(filename) {
  const content = fs.readFileSync(filename, "utf-8");

  // Use the babylon parser
  // to generate an AST
  const ast = parse(content, { sourceType: "module" });

  const dependencies = [];

  // traverse the AST to try and understand which module the current module depends on
  // to do that, check every import declaration in the AST
  traverse(ast, {
    // EcmaScript modules are fairly because they are static
    // You cannot import a variable or conditionally import another module
    // Everytime we see an import statement we can just count its value as a dependency
    ImportDeclaration: ({ node }) => {
      dependencies.push(node.source.value);
    },
  });

  // unique identifier to this module
  const id = ID++;

  // make sure the bundle runs in all browsers
  // transpile it with Babel
  // transformFromAst: transform an AST
  const { code } = transformFromAst(ast, null, {
    presets: ["@babel/preset-env"],
  });

  return {
    id,
    filename,
    dependencies,
    code,
  };
}

// Extract the dependencies of the entry file
function createDependencyGraph(entryPoint) {
  const mainAsset = createAsset(entryPoint);

  // use queue
  // BFS
  const queue = [mainAsset];
  for (const asset of queue) {
    // every asset has a list of relative paths to the modules it depends on
    // we visualize this data structure as a "map"
    // module map
    asset.mapping = {};

    const dirname = path.dirname(asset.filename);

    // iterate over the list of relatives paths to its dependencies
    asset.dependencies.forEach((relativePath) => {
      // the `createAsset()` function expects an absolute filename
      // the dependencies array is a list of _relative_ paths, which are relative to the file that imported them
      // we can turn the relative path into an absolute one by joining it with the path to the directory of the parent asset
      const absolutePath = path.join(dirname, relativePath);

      // parse the asset, read its content, and extract its dependencies
      const childModule = createAsset(absolutePath);
      asset.mapping[relativePath] = childModule.id;

      queue.push(childModule);
    });
  }

  // At this point the queue is just an array with every module in the target application
  return queue;
}

// Define a function that uses the graph and returns a bundle that can be run in browser
function bundle(graph) {
  let modules = "";

  graph.forEach((mod) => {
    // every module in the graph has an entry
    // `[key]: value`
    // every key has two values:
    //   - code of each module wrapped within a function (because modules should be scoped: defining a variable in one module should not affect others or the global scope)
    //     - the modules, after being transpiled, use the CommonJS module system - they expect a `require`, a `module`, and an `exports` objects to be present
    //     - these are not normally available in the browser so we will implement them and inject them into the function wrappers
    //   - stringify the mapping between the module and its dependencies
    modules += `${mod.id}: [
      function (require, module, exports) {
        ${mod.code}
      },
      ${JSON.stringify(mod.mapping)},
    ],`;
  });

  const result = `
    (function (modules) {
      function require(id) {
        const [fn, mapping] = modules[id];

        function localRequire(name) {
          return require(mapping[name]); 
        }

        const module = {exports: {}};
        fn(localRequire, module, module.exports);

        return module.exports;
      }

      require(0);
    })({${modules}})
  `;

  return result;
}

const graph = createDependencyGraph("../test/entry.js");
const result = bundle(graph);

console.log(result);
